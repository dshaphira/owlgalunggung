/* OwlGalunggung Code Editor
 * owlgaltextview2_spell.h
 *
 * Copyright (C) 2017-2018 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

/* for the design docs see owlgaltextview2.h */
#ifndef _OWLGALTEXTVIEW2_SPELL_H_
#define _OWLGALTEXTVIEW2_SPELL_H_

#include "owlgaltextview2.h"
void unload_spell_dictionary(Towlgalwin * owlgalwin);
gboolean owlgaltextview2_run_spellcheck(OwlGalunggungTextView * btv);
void owlgaltextview2_spell_init(void);
void owlgaltextview2_spell_cleanup(void);
void owlgaltextview2_populate_suggestions_popup(GtkMenu * menu, Tdocument * doc);
void owlgaltextview2_populate_preferences_popup(GtkMenu * menu, Tdocument * doc);
void owlgaltextview2_gui_toggle_spell_check(GtkWidget * widget, gpointer data);
void reload_spell_dictionary(Towlgalwin * owlgalwin);
#endif
